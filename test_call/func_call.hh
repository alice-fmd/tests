#ifndef FUNC_CALL_HH
#define FUNC_CALL_HH

//____________________________________________________________________
enum VariableType {
  /** Integer */
  INTEGER_VAR, 
  /** Floating point */
  FLOAT_VAR, 
  /** Nothing */
  VOID_VAR
};

//____________________________________________________________________
template <typename T>
const char* type2param();

template <> const char* type2param<bool>() { return "bool"; }
template <> const char* type2param<int>() { return "int"; }
template <> const char* type2param<unsigned int>() { return "unsigned"; }
template <> const char* type2param<float>() { return "float"; }
template <> const char* type2param<unsigned int*>() { return "dyn_uint"; }


//____________________________________________________________________
struct FunctionListRec
{
  VaribleType retType;
  char*       name;
  char*       paramList;
  bool        threadedCallPossible;
};



//____________________________________________________________________
struct InterfaceDeclBase 
{
  InterfaceDeclBase(VariableType ret, 
		    const char*  name, 
		    bool         threadsafe) 
  {
    fRec.retType              = ret;
    fRec.name                 = name;
    fRec.threadedCallPossible = threadsafe;
  };
  FunctionListRec& GetRecord() { return fRec; }
  virtual void MakeParamString() = 0;
protected:
  FunctionListRec fRec;
  std::string     fParamList;
};

//____________________________________________________________________
template <VariableType Return, 
	  typename T
	  typename Field01, 
	  typename Field02, 
	  typename Field03>
struct ReadInterfaceDecl : public InterfaceDeclBase 
{
  typedef T InterfaceType;
  typedef Field01 (InterfaceType::*)() const GetField01Type;
  typedef Field02 (InterfaceType::*)() const GetField02Type;
  typedef Field03 (InterfaceType::*)() const GetField03Type;
  
  InterfaceDecl(const char* name, 
		GetField01Type get1,
		GetField01Type get2,
		GetField01Type get3,
		bool threadsafe) 
    : InterfaceDecl(Return, name, threadsafe), 
      fGet1(get1), 
      fGet2(get2), 
      fGet3(get3)
  {
  }
  void operator()(T* f) 
  {
    Field01 f1 = (f->*fGet1)();
    Field02 f2 = (f->*fGet2)();
    Field03 f3 = (f->*fGet3)();

    std::cout << name  << "\t"
	      << "f1=" << f1 << "\t"
	      << "f2=" << f2 << "\t" 
	      << "f3=" << f3 << std::endl;
  }
  void MakeParamString() { 
    std::stringstream s(fParamString);
    s << "(";
    if (fGet1) { s << type2param<Field01>() << " p1";
      if (fGet2) { s << ", " << type2param<Field02>() << " p2";
	if (fGet2) s << ", " << type2param<Field03>() << " p3";
      }
    }
    s << ")";
  }
  
	  
	
  

    


    
    





#endif
//
// EOF
//

  
    
