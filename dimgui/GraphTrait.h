// -*- mode: C++ -*-
#ifndef GRAPH_TRAITS_H
#define GRAPH_TRAITS_H
#include <TGNumberEntry.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGButton.h>
#include <TH1.h>
#include <TMath.h>
#include <limits>

//____________________________________________________________________
/** Frame that shows the service listen to in frame, and shows a trend
    of the service in a canvas */
struct GraphFrame : public TGVerticalFrame
{
  /** Static function to make unique id's for the canvases */
  static int MakeId() 
  {
    static int id = 0;
    return id++;
  }
  /** Constructor. 
      @param p     Parent frame 
      @param name  Name of service 
      @param style Type of number 
      @param min   Minimum value 
      @param max   Maximum value */
  GraphFrame(TGCompositeFrame* p, const char* name,
	     TGNumberFormat::EStyle style, Int_t col=0, 
	     Double_t min=0, Double_t max=INT_MAX)
    : TGVerticalFrame(p), 
      fTop(this),
      fPause(&fTop, "Pause"),
      fClear(&fTop, "Clear"),
      fEntry(&fTop, -1, 0, style, 
	     TGNumberFormat::kNEAAnyNumber, 
	     TGNumberFormat::kNELLimitMinMax, min, max), 
      fEmCanvas(0, this, 400, 200),
      fCanvas(Form("c%02d",MakeId()), 10, 10, fEmCanvas.GetCanvasWindowId()), 
      fStart(time(NULL))
  {
    // Add frames 
    fTop.AddFrame(&fEntry, new TGLayoutHints(kLHintsExpandX));
    fTop.AddFrame(&fPause);
    fTop.AddFrame(&fClear);
    AddFrame(&fTop, new TGLayoutHints(kLHintsExpandX));
    AddFrame(&fEmCanvas, new TGLayoutHints(kLHintsExpandX,kLHintsExpandY));

    // Buttons stay down 
    fPause.AllowStayDown(kTRUE);
    fClear.AllowStayDown(kTRUE);

    // Size of field 
    Int_t logmax = Int_t(TMath::Log10(max)) + 1;
    fEntry.SetMaxLength((logmax > 1 ? logmax : 1));
    fEntry.SetEditDisabled();
    fEntry.SetEnabled(kFALSE);

    // Canvas 
    fEmCanvas.AdoptCanvas(&fCanvas);
    fCanvas.cd();
    fCanvas.SetBorderMode(0);
    fCanvas.SetBorderSize(0);
    fCanvas.SetFillColor(0);
    fCanvas.SetBottomMargin(0.10);
    fCanvas.SetLeftMargin(0.10);
    fCanvas.SetTopMargin(0.05);
    fCanvas.SetRightMargin(0.05);

    // Graph
    fGraph.Draw("apl");
    fGraph.SetLineColor(col);
    fGraph.SetMarkerColor(col);
    fGraph.SetMarkerStyle(20);
    fGraph.GetHistogram()->GetXaxis()->SetTimeFormat();
    fGraph.GetHistogram()->SetMinimum(-.5);
    fGraph.GetHistogram()->GetXaxis()->SetTitle("t [s]");
    fGraph.GetHistogram()->GetYaxis()->SetTitle(name);
  }
  /** Update the value. 
      @param t Time since epoch 
      @param v New value */
  void Update(int t, Double_t v)
  {
    static int last = t;
    if (t == last) return;
    if (fClear.IsDown()) { 
      fClear.SetDown(kFALSE);
      int n = fGraph.GetN();
      for (int i = n-1; i >= 0; i--) fGraph.RemovePoint(i);
    }
    if (fPause.IsDown()) return;
    fEntry.SetNumber(v);
    fGraph.SetPoint(fGraph.GetN(), t-fStart, v);
    fCanvas.Modified();
    fCanvas.Update();
    fCanvas.cd();
  }
  /** Top frame */
  TGHorizontalFrame  fTop;
  /** Button */
  TGTextButton       fPause;
  /** Button */
  TGTextButton       fClear;
  /** View */
  TGNumberEntryField fEntry;
  /** Canvas */
  TRootEmbeddedCanvas fEmCanvas;
  /** Canvas */
  TCanvas fCanvas;
  /** Graph */
  TGraph fGraph;
  /** Start time */
  int fStart;
};
  
//____________________________________________________________________
/** Trait that shows the number in a field and in a graph */
struct IntGraphTrait 
{
  /** GEt address of @param x */
  static void*    Address(const int& x) 
  { 
    return reinterpret_cast<void*>(&(const_cast<int&>(x))); 
  }
  /** Get size of x */
  static size_t   Size(int x) { return sizeof(x); }
  /** Get value of service */
  static int      Value(DimInfo& info) { return info.getInt(); }
  /** Compare x to y */
  static int      Compare(int x, int y) { return x == y; }
  /** Mak GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name) 
  {
    return new GraphFrame(p, name, TGNumberFormat::kNESInteger, kRed);
  }
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i) { 
    GraphFrame* d = dynamic_cast<GraphFrame*>(f);
    if (!d) { 
      std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
      return;
    }
    d->Update(i.getTimestamp(), Value(i));
  }
};
//____________________________________________________________________
struct FloatGraphTrait 
{
  /** GEt address of @param x */
  static void*    Address(const float& x) 
  { 
    return reinterpret_cast<void*>(&(const_cast<float&>(x))); 
  }
  /** Get size of x */
  static size_t   Size(float x) { return sizeof(x); }
  /** Get value of service */
  static float      Value(DimInfo& info) { return info.getFloat(); }
  /** Compare x to y */
  static float      Compare(float x, float y) { return x == y; }
  /** Mak GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name) 
  {
    return new GraphFrame(p, name, TGNumberFormat::kNESReal, kBlue);
  }
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i) { 
    GraphFrame* d = dynamic_cast<GraphFrame*>(f);
    if (!d) { 
      std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
      return;
    }
    d->Update(i.getTimestamp(), Value(i));
  }
};
#endif
