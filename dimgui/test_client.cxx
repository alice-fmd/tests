#include <iostream>
#include <string>
#include "Main.h"
#include "Traits.h"
#include "GraphTrait.h"

//____________________________________________________________________
int main(int argc, char** argv)
{
  std::string dnsNode = "localhost";
  std::string name    = "TEST";
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') { 
      switch (argv[i][1]) { 
      case 'd': dnsNode = argv[++i]; break;
      case 'n': name    = argv[++i]; break;
      case 'h': 
	std::cout << "Usage: " << argv[0] << " [-d DNS] [-n NAME]" << std::endl;
	return 0;
      }
    }
  }

  TApplication app(Form("%s_client", name.c_str()), 0, 0);
  Main         m(dnsNode.c_str(), name.c_str());
  m.AddService<int,IntGraphTrait>("INTVAL",-1, 1); 
  m.AddService<float,FloatGraphTrait>("FLOATVAL",-1); 
  m.AddService<bool,BoolTrait>("BOOLVAL",0); 
  m.Display();
  
  return 0;
}
