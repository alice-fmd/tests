// -*- mode: C++ -*-
#ifndef INFO_DISPLAY_H
#define INFO_DISPLAY_H
#include <TGFrame.h>
#include <TGLabel.h>
#include <dic.hxx>
#include <iostream>

//____________________________________________________________________
/** BAse class for service display */
class InfoDisplayBase : public TGGroupFrame
{
public:
  /** Constructor 
      @param p Parent 
      @param name Name */
  InfoDisplayBase(TGCompositeFrame* p, const char* name) 
    : TGGroupFrame(p, name, kVerticalFrame), 
      fParent(p),
      fName(name), 
      fStatus(this, "No link")
  {
    AddFrame(&fStatus, new TGLayoutHints(kLHintsExpandX, 3, 3, 3, 3));
  }
protected:
  /** Set status display */
  void SetStatus(const char* what) { 
    fStatus.SetText(what);
    gClient->NeedRedraw(&fStatus, kTRUE);
    gClient->NeedRedraw(this, kTRUE);
    DoRedraw();
  }
  void SetNoLink()  { SetStatus("No link");  } 
  void SetUpdated() { SetStatus("Updated"); }
  void SetOk()      { SetStatus("OK"); }
  std::string fName;
  TGLabel     fStatus;
  TGFrame*    fParent;
};

//____________________________________________________________________
/** Class template for showing a service */
template <typename T, typename Trait>
class InfoDisplay : public InfoDisplayBase, public DimInfoHandler 
{
public:
  /** Constructor 
      @param p PArent 
      @param name Name 
      @param nolink Value on no link 
      @param timeout Timeout (if 0, never time out) */
  InfoDisplay(TGCompositeFrame* p, const char* name, 
	      const T& nolink, int timeout)

    : InfoDisplayBase(p, name), 
      fNoLink(nolink),
      fInfo(name, timeout, Trait::Address(nolink), Trait::Size(nolink), this), 
      fFrame(Trait::MakeFrame(this, name))
  {
    AddFrame(fFrame, new TGLayoutHints(kLHintsExpandX, 3, 3, 3, 3));
  }
  /** HAndle updtes */
  void infoHandler()
  {
    // std::cout << fName << " updated" << std::endl;
    T val = Trait::Value(fInfo);
    Trait::UpdateFrame(fFrame, fInfo);
    gClient->NeedRedraw(fFrame, kTRUE);
    if      (Trait::Compare(fNoLink, val)) SetNoLink();
    else if (Trait::Compare(val, fValue))  SetOk();
    else                                   SetUpdated();
    fValue = val;
  }
protected:
  DimStampedInfo  fInfo;
  const T  fNoLink;
  T        fValue;
  TGFrame* fFrame;
};
#endif
//
// EOF
//
