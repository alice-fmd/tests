/** @mainpage FeeServer implemented in C++ 

    This is a C++ implementation of the %FeeServer.

    @section contents Contents 
    - @ref doc_main     (see also @ref core)
    - @ref doc_channels (see also @ref channels and @ref packets)
    - @ref doc_proc     (see also @ref processes)
      - @ref doc_mon
      - @ref doc_watch
      - @ref doc_issue 
    - @ref doc_scmd     (see also @ref scmd)
    - @ref doc_ce       (see also @ref ctrl_engine)
      - @ref doc_serv   (see also @ref services) 
    - @ref doc_client   (see also @ref client)
    - @ref threads 
    - Examples
      - @ref test_server.cc
      - @ref test_client.cc


    It's a complete re-implementation of the original C %FeeServer 
    (see @ref links). 

    The focus of this implementation is to make the code simple and
    clear, and provide a simple interface for customisation of the
    %FeeServer. 

    @section doc_main Server

    The basic object of the %FeeServer is the class FeeServer::Main.  A
    user should always define an object of this type.  Alternatively,
    one can derive a user class from this class for doing more stuff
    (code from @ref test_server.cc). 

    @dontinclude test_server.cc
    @skipline FeeServer::Main m
    
    @section doc_channels Channels 

    The basic FeeServer::Main class provides the 3 DIM channels 
    - <i>name</i><tt>_Message</tt> (see @ref FeeServer::MessageChannel)
    - <i>name</i><tt>_Command</tt> (see @ref FeeServer::CommandChannel)
    - <i>name</i><tt>_Acknowledge</tt> (ses @ref
      FeeServer::AcknowledgeChannel) 

    where <i>name</i> is the name of the server as set in the
    constructor of FeeServer::Main. 

    The command channel provides a way for clients to send commands to
    the server.  The command contains a header (see @ref
    FeeServer::Header) followed by binary large object (BLOB) of data
    (see @ref FeeServer::Command).  The header contains a few pieces
    of information for the server to use.  The meaning of the binary
    data depends entirely on the command send and the implementation
    of the server.

    The acknowledge channel provides the feed back channel from the
    command handling.  After a command is processed, this channel is
    updated.  The channel contains a header (see @ref
    FeeServer::Header) followed by binary large object (BLOB) of data
    (see @ref FeeServer::Acknowledge). The meaning of the BLOB depends
    entirely on the command sent and the implementation of the server.
    The header will have the same identifier field as the processed
    command, and contain a status code.

    The message channel provides various messages about the operations
    of the server (see @ref FeeServer::Message).  The type of messages
    that are send is configurable. 

    @skip if (debug)
    @until }

    @section doc_proc Proccess in the server 

    The server runs several process in its process. 

    @subsection doc_mon Monitoring 

    A separate thread is running the monitoring (see @ref
    FeeServer::MonitorThread).  Each time a registered service is
    updated, a corresponding DIM service will be updated.  The
    frequency of forces updates can be configured.

    @subsection doc_watch Message watch dog

    A separate thread is making sure that messages are send to the
    clients (see @ref FeeServer::MessageWatchdog) and that the channel
    isn't flooded.

    @subsection doc_issue Issue thread

    Handling of control engine commands is done in a separate thread
    to allow the server to continue monitoring services while handling
    commmands (see @ref FeeServer::IssueThread).

    

    @section doc_scmd Server commands 
    
    As it stands, the FeeServer::Main object has no commands what so
    ever.  However,the abstract class FeeServer::ServerCommand
    provides an interface for defining server commands.  A number of
    commands is defined in the library (see @ref scmd). 

    You can simple create objects of the server command type, and then
    add them to the server using the member function
    FeeServer::Main::AddCommand. 
    
    @skip // m.AddCommand
    @until new FeeServer::GetLogLevel
    
    @section doc_ce Control Engine 
    
    To control actual hardware or other stuff in the server machine,
    one needs to define a @e Control @e Engine.  The library provides
    the abstract class FeeServer::ControlEngine that the user should
    derive from and implement.  

    @dontinclude test_server.cc 
    @skip namespace Example 
    @until }

    The derived class should do periodic stuff in the
    FeeServer::ControlEngine::Start member function.

    @until } //End-Start
    @note It's crucial that the member function
    FeeServer::ControlEngine::Ready is called from this member
    function.  The main object waits for the control engine to signal
    that it is ready, and the control engine should wait until the
    FeeServer::Main object is ready.  The
    FeeServer::ControlEngine::Ready does both of this. 

    Commands for the control engine should be handled in the
    FeeServer::ControlEngine::Issue member function. 
    
    @until } //End-Issue
    
    The member function FeeServer::ControlEngine::CleanUp is called
    when the server exists.

    @until CleanUp

    @subsection doc_serv Services 
    
    The control engine can define a number of services by declaring
    objects of sub-classes of the abstract class
    FeeServer::ServiceBase.  The services should be added to some
    FeeServer::ServiceHandler on which a periodic call to
    FeeServer::ServiceHandler::UpdateAll is done. 

    The library provides a class template (see FeeServer::ServiceT)
    for defining types, as well as some specialisation for common
    types (FeeServer::IntService, FeeServer::FloatService).

    @until //End-Example

    The control engine should then be registered with the
    FeeServer::Main object, and we can execute the server. 

    @skip Examples::TestControlEngine
    @until m.Run();

    @section doc_client Simple client 

    The class FeeServer::Client provides a simple client.  For more
    elaborate stuff, one could extract this client (in source files
    fee_client.hh and fee_client.cc) and the packet definitions (in
    source file fee_packet.hh) from this library and use it in some
    client code.   The client provides a number of member functions to
    handle messages, acknowledges, and as well as missing services,
    and to send commands to the server (see @ref test_client.cc). 


    @section links Links 
    - <a href="http://web.ift.uib.no/~kjeks/wiki/">Bergen DCS
        pages</a>
    - <a href="http://www.ztt.fh-worms.de/en/projects/Alice-FEEControl/">Worms
        ZTT Fee pages</a>
    - <a href="https://www.ztt.fh-worms.de/download/alice/">Worms ZTT
        download page</a> (password protected)


*/
#error not for compilation. 

//
// EOF
//
