#include <iostream>
#include <dim/dis.hxx>
#ifndef WIN32
# include <unistd.h>
#endif
#include <string>

//____________________________________________________________________
class ErrorHandler : public DimErrorHandler
{
public:
  ErrorHandler() {DimServer::addErrorHandler(this);}
protected:
  void errorHandler(int severity, int code, char *msg)
  {
    int    index = 0;
    char** services = DimServer::getClientServices();
    std::cout << severity << " " << msg << "\n"
	      << "From "<< DimServer::getClientName() 
	      << " services:" << std::endl;
    while(services[index]) {
      std::cout << "\t" << services[index] << std::endl;
      index++;
    }
  }
};

//____________________________________________________________________
class ExitHandler : public DimExitHandler
{
  void exitHandler(int code)
  {
    std::cout << "exit code " << code << std::endl;
  }
public:
  ExitHandler() {DimServer::addExitHandler(this);}
};

//____________________________________________________________________
char*
makeName(const std::string& name, const char* what)
{
  static char buf[1024];
  sprintf(buf, "%s/%s", name.c_str(), what);
  return buf;
}

//____________________________________________________________________
int main(int argc, char** argv)
{
  std::string dnsNode = "localhost";
  std::string name    = "TEST";
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') { 
      switch (argv[i][1]) { 
      case 'd': dnsNode = argv[++i]; break;
      case 'n': name    = argv[++i]; break;
      case 'h': 
	std::cout << "Usage: " << argv[0] << " [-d DNS] [-n NAME]" << std::endl;
	return 0;
      }
    }
  }
  DimServer::setDnsNode(dnsNode.c_str());
  int              ival    = 0;
  float            fval    = 0;
  std::string      s1      = "hello";
  bool             boolval = false;
  DimService       servint(makeName(name,"INTVAL"),ival);
  DimService       servfloat(makeName("TEST","FLOATVAL"), fval);
  DimService       servbool(makeName("TEST","BOOLVAL"),"C:1", 
			    (void *)&boolval, 1);
  DimService       servstr(makeName("TEST","STRINGVAL"),(char *)s1.c_str());
  
  DimServer::start(name.c_str());

  while(true) {
    sleep(3);
    s1      =  "hello1";
    boolval =  !boolval;
    ival    =  rand() % 10;
    fval    =  float(rand())/RAND_MAX; 
    servint.setTimestamp(time(NULL),0);
    servint.updateService();
    servfloat.updateService();
    servbool.updateService();
  }
  return 0;
}

