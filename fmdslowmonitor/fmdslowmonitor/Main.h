// -*- mode: C++ -*-
#ifndef MAIN_H
#define MAIN_H
#include <TGFrame.h>
#include <dim/dic.h>
#include <TApplication.h>
#include <string>
#include <iostream>
#include <vector>
#include "InfoDisplay.h"

//____________________________________________________________________
/** Main frame */
class Main : public TGMainFrame, public DimErrorHandler
{
public:
  /** Constructor 
      @param dns DIM Domain name server 
      @param name NAme of Server */
  Main(const char* dns, const char* name) 
    : TGMainFrame(gClient->GetRoot(), 300, 400, kVerticalFrame), 
      fName(name)
  {
    DimClient::setDnsNode(dns);
    DimClient::addErrorHandler(this);
  }
  /** Terminate */
  void CloseWindow()
  {
    gApplication->Terminate();
  }
  template <typename T> 
  void AddService(const char* name, int timeout=0)
  {
    AddService<T,DisplayTrait<T> >(name, InfoTrait<T>::NoLink(), timeout);
  }
  template <typename T> 
  void AddService(const char* name, 
		  const typename InfoTrait<T>::NoLinkType& nolink, 
		  int timeout)
  {
    AddService<T,DisplayTrait<T> >(name, nolink, timeout);
  }
  
  /** Add a service. 
      @param T  Type of service 
      @param Trait Trait of display 
      @param name Name of service 
      @param nolink No link value 
      @param timeout Timeout (in seconds, if 0 no timeout) */
  template <typename T, typename Trait>
  void AddService(const char* name, 
		  const typename InfoTrait<T>::NoLinkType& nolink, int timeout)
  {
    InfoDisplayBase* d = 
      new InfoDisplay<T,Trait>(this, Form("%s/%s", fName.c_str(), name), 
			       timeout, nolink);
    AddFrame(d, new TGLayoutHints(kLHintsExpandX, 3, 3, 3, 3));
    fServices.push_back(d);
  }
  /** Show it */
  void Display()
  {
    MapSubwindows();
    Resize(GetDefaultSize());
    MapWindow();
    gApplication->Run();
  }
protected:
  std::string fName;
  void errorHandler(int severity, int code, char *msg)
  {
    int index = 0;
    char** services = DimClient::getServerServices();
    std::cout << severity << " " << msg << "\n"
	      << "from "<< DimClient::getServerName() 
	      << " services:" << std::endl;
    while(services[index]) {
      std::cout << "\t" << services[index] << std::endl;
      index++;
    }
  }

  std::vector<InfoDisplayBase*> fServices;
};

#endif
//
// EOF
//
