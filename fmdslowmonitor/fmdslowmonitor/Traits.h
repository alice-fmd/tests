// -*- mode: C++ -*-
#ifndef TRAITS_H
#define TRAITS_H
#include <TGNumberEntry.h>
#include <TGButton.h>
#include <limits>

//____________________________________________________________________
/** Trait that shows an integer */
struct IntTrait 
{
  /** GEt address of @param x */
  static void*    Address(const int& x) 
  { 
    return reinterpret_cast<void*>(&(const_cast<int&>(x))); 
  }
  /** Get size of x */
  static size_t   Size(int x) { return sizeof(x); }
  /** Get value of service */
  static int      Value(DimInfo& info) { return info.getInt(); }
  /** Compare x to y */
  static int      Compare(int x, int y) { return x == y; }
  /** Make GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name) 
  {
    return new TGNumberEntryField(p, -1, 0, 
				  TGNumberFormat::kNESInteger, 
				  TGNumberFormat::kNEAAnyNumber, 
				  TGNumberFormat::kNELLimitMinMax, 0, INT_MAX);
  }
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i) { 
    TGNumberEntryField* d = dynamic_cast<TGNumberEntryField*>(f);
    if (!d) { 
      std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
      return;
    }
    d->SetIntNumber(Value(i));
  }
};

//____________________________________________________________________
struct FloatTrait 
{
  /** GEt address of @param x */
  static void*    Address(const float& x) 
  { 
    return reinterpret_cast<void*>(&(const_cast<float&>(x))); 
  }
  /** Get size of x */
  static size_t   Size(float x) { return sizeof(x); }
  /** Get value of service */
  static float    Value(DimInfo& info) { return info.getFloat(); }
  /** Compare x to y */
  static float    Compare(float x, float y) { return x == y; }
  /** Make GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name) 
  {
    return new TGNumberEntryField(p, -1, 0, 
				  TGNumberFormat::kNESReal, 
				  TGNumberFormat::kNEAAnyNumber, 
				  TGNumberFormat::kNELLimitMinMax, 0, 
				  std::numeric_limits<float>::max());
  }
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i) { 
    TGNumberEntryField* d = dynamic_cast<TGNumberEntryField*>(f);
    if (!d) { 
      std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
      return;
    }
    d->SetNumber(Value(i));
  }
};

//____________________________________________________________________
struct BoolTrait 
{
  /** GEt address of @param x */
  static void*    Address(const bool& x) 
  { 
    return reinterpret_cast<void*>(&(const_cast<bool&>(x))); 
  }
  /** Get size of x */
  static size_t   Size(bool x) { return sizeof(x); }
  /** Get value of service */
  static bool     Value(DimInfo& info) 
  { 
    void* data = info.getData();
    bool  val  = *(reinterpret_cast<bool*>(data));
    return val;
  }
  /** Compare x to y */
  static bool     Compare(bool x, bool y) { return x == y; }
  /** Mak GUI element */
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name) 
  {
    return new TGTextButton(p, "");
  }
  /** Update GUI element */
  static void     UpdateFrame(TGFrame* f, DimInfo& i) { 
    TGTextButton* d = dynamic_cast<TGTextButton*>(f);
    if (!d) { 
      std::cerr << "Frame is not a TGTextButton!" << std::endl;
      return;
    }
    d->ChangeBackground(Value(i) ? 0x00ff00 : 0xff0000);
  }
};

#endif
//
// EOF
//


