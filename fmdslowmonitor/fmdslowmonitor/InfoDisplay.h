// -*- mode: C++ -*-
#ifndef INFO_DISPLAY_H
#define INFO_DISPLAY_H
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <fmdslowmonitor/Info.h>
#include <iostream>

//____________________________________________________________________
/** Base class for service display */
class InfoDisplayBase : public TGGroupFrame
{
public:
  /** Constructor 
      @param p Parent 
      @param name Name */
  InfoDisplayBase(TGCompositeFrame* p, const char* name);
  void SetFrame(TGFrame* f, TGLayoutHints* hints=0);
protected:
  /** Set status display */
  void SetStatus(int col);
  void SetNoLink()  { SetStatus(0xaa0000); } 
  void SetUpdated() { SetStatus(0x00aa00); }
  void SetOk()      { SetStatus(0x0000aa); }
  std::string        fName;
  // TGLabel         fStatus;
  TGFrame*           fParent;
  TGHorizontalFrame  fCont;
  // TGTextButton       fStatus;
};

//____________________________________________________________________
template <typename T>
struct DisplayTrait
{
  /** Value type */
  typedef T                         ValueType;
  /** Type of info trait */
  typedef InfoTrait<ValueType>      TraitType;
  /** Type of Info */
  typedef Info<ValueType,TraitType> InfoType;
  
  /** Make a  frame 
      @param  p    Parent frame 
      @param  name Name 
      @return newly allocated frame to display a service of this type */ 
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name);
  /** Update a frame wit the current value 
      @param f  Frame to update 
      @param i  Information to update from */
  static void UpdateFrame(TGFrame* f, const InfoType& i);
};

//____________________________________________________________________
template <>
struct DisplayTrait<int>
{
  /** Type of info trait */
  typedef InfoTrait<int>      TraitType;
  /** Type of Info */
  typedef Info<int,TraitType> InfoType;

  /** Make a  frame 
      @param  p    Parent frame 
      @param  name Name 
      @return newly allocated frame to display a service of this type */ 
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name)
  {
    return new TGNumberEntryField(p, -1, 0, 
				  TGNumberFormat::kNESInteger, 
				  TGNumberFormat::kNEAAnyNumber, 
				  TGNumberFormat::kNELLimitMinMax, 0, INT_MAX);
  }
  /** Update a frame wit the current value 
      @param f  Frame to update 
      @param i  Information to update from */
  static void UpdateFrame(TGFrame* f, const InfoType& i) 
  { 
    TGNumberEntryField* d = dynamic_cast<TGNumberEntryField*>(f);
    if (!d) { 
      std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
      return;
    }
    d->SetNumber(i.Value());
  }
};
//____________________________________________________________________
template <>
struct DisplayTrait<float>
{
  /** Type of info trait */
  typedef InfoTrait<float>      TraitType;
  /** Type of Info */
  typedef Info<float,TraitType> InfoType;
  /** Make a  frame 
      @param  p    Parent frame 
      @param  name Name 
      @return newly allocated frame to display a service of this type */ 
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name)
  {
    return new TGNumberEntryField(p, -1, 0, 
				  TGNumberFormat::kNESReal, 
				  TGNumberFormat::kNEAAnyNumber, 
				  TGNumberFormat::kNELLimitMinMax, 0, INT_MAX);
  }
  /** Update a frame wit the current value 
      @param f  Frame to update 
      @param i  Information to update from */
  static void UpdateFrame(TGFrame* f, const InfoType& i) 
  { 
    TGNumberEntryField* d = dynamic_cast<TGNumberEntryField*>(f);
    if (!d) { 
      std::cerr << "Frame is not a TGNumberEntryField!" << std::endl;
      return;
    }
    d->SetNumber(i.Value());
  }
};
//____________________________________________________________________
template <>
struct DisplayTrait<bool>
{
  /** Type of info trait */
  typedef InfoTrait<bool>      TraitType;
  /** Type of Info */
  typedef Info<bool,TraitType> InfoType;
  
  /** Make a  frame 
      @param  p    Parent frame 
      @param  name Name 
      @return newly allocated frame to display a service of this type */ 
  static TGFrame* MakeFrame(TGCompositeFrame* p, const char* name)
  {
    return new TGTextButton(p, "");
  }
  /** Update a frame wit the current value 
      @param f  Frame to update 
      @param i  Information to update from */
  static void UpdateFrame(TGFrame* f, const InfoType& i) 
  { 
    TGTextButton* d = dynamic_cast<TGTextButton*>(f);
    if (!d) { 
      std::cerr << "Frame is not a TGTextButton!" << std::endl;
      return;
    }
    d->ChangeBackground(i.Value() ? 0x00ff00 : 0xff0000);
  }
};

//____________________________________________________________________
/** Class template for showing a service */
template <typename T, typename Trait=DisplayTrait<T> >
class InfoDisplay : public InfoDisplayBase, public DimInfoHandler 
{
public:
  typedef Trait                         TraitType;
  typedef typename Trait::InfoType      InfoType;
  typedef typename Trait::TraitType     InfoTraitType;
  typedef typename InfoType::NoLinkType NoLinkType;
  typedef typename InfoType::ValueType  ValueType;
  
  /** Constructor 
      @param p PArent 
      @param name Name 
      @param nolink Value on no link 
      @param timeout Timeout (if 0, never time out) */
  InfoDisplay(TGCompositeFrame* p, const char* name, 
	      int timeout=0, const NoLinkType& nolink=InfoTraitType::NoLink())
    : InfoDisplayBase(p, name), 
      fInfo(name, this, timeout, nolink), 
      fFrame(TraitType::MakeFrame(&fCont, name))
  {
    SetFrame(fFrame, new TGLayoutHints(kLHintsExpandX, 3, 3, 3, 3));
  }
  /** Handle updates */
  void infoHandler()
  {
    gClient->NeedRedraw(fFrame, kTRUE);
    if (!fInfo.IsUp()) { 
      SetNoLink();
      return;
    }
    ValueType val = fInfo.Value();
    TraitType::UpdateFrame(fFrame, fInfo);
    if   (InfoTraitType::Compare(val, fValue))  SetOk();
    else                                        SetUpdated();
    fValue = val;
  }
protected:
  InfoType          fInfo;
  ValueType         fValue;
  TGFrame*          fFrame;
};
#endif
//
// EOF
//
