#include "InfoDisplay.h"
#include <TGTableLayout.h>

//____________________________________________________________________
InfoDisplayBase::InfoDisplayBase(TGCompositeFrame* p, const char* name) 
  : TGGroupFrame(p, name, kHorizontalFrame), 
    fParent(p),
    fCont(this),
    fName(name) //, 
    // fStatus(this, "")
{
  // fStatus.SetMinHeight(20);
  // fStatus.SetMinWidth(20);
  //   SetLayoutManager(new TGTableLayout(this, 1, 2, kFALSE, 3));
  // AddFrame(&fStatus, new TGLayoutHints(kLHintsTop|kLHintsLeft));
  AddFrame(&fCont,   new TGLayoutHints(kLHintsExpandY|kLHintsExpandX));
  // fStatus.SetMinHeight(20);
  // fStatus.SetMinWidth(20);
}

//____________________________________________________________________
void 
InfoDisplayBase::SetFrame(TGFrame* f, TGLayoutHints* hints)
{
  fCont.AddFrame(f, hints);
}

//____________________________________________________________________
void 
InfoDisplayBase::SetStatus(Int_t col) 
{ 
  SetTextColor(col);
  // fStatus.ChangeBackground(col);
  // gClient->NeedRedraw(&fStatus, kTRUE);
  gClient->NeedRedraw(this, kTRUE);
  DoRedraw();
}

//____________________________________________________________________
//
// EOF
//
