// -*- mode: C++ -*-
#ifndef INFO_H
#define INFO_H
#include <dim/dic.hxx>
#include <string>
#include <iostream>
//____________________________________________________________________
/** Class template that wraps types */
template <typename T>
struct InfoTrait
{
  /** Value type */
  typedef T ValueType;
  /** No link type */
  typedef T NoLinkType;
  /** Create the default no-link value */
  static NoLinkType NoLink() { return ValueType(); }
  /** Get the address of the passed value */ 
  static void* Address(const ValueType& x) { 
    return reinterpret_cast<void*>(&(const_cast<ValueType&>(x)));
  }
  /** Get the size of passed value */
  static size_t Size(const ValueType& x) { return sizeof(x); }
  /** Get the value of the service */ 
  static ValueType Value(DimInfo& i) { 
    return *(reinterpret_cast<ValueType*>(i.getData()));
  }
  /** Check if info value is the same as no-link */
  static bool IsUp(const NoLinkType& nolink, DimInfo& i)
  {
    return nolink != Value(i);
  }
  /** Compare for equality */
  static bool Compare(const ValueType& x, const ValueType& y) 
  {
    return x == y;
  }
};

//____________________________________________________________________
/** Class template that wraps types */
template <>
struct InfoTrait<bool>
{
  /** Value type */
  typedef bool ValueType;
  /** No link type */
  typedef char NoLinkType;
  /** Create the default no-link value */
  static NoLinkType NoLink() { return 'z'; }
  /** Get the address of the passed value */ 
  static void* Address(const ValueType& x) { 
    return reinterpret_cast<void*>(&(const_cast<ValueType&>(x)));
  }
  /** Get the address of the passed value */ 
  static void* Address(const NoLinkType& x) { 
    return reinterpret_cast<void*>(&(const_cast<NoLinkType&>(x)));
  }
  /** Get the size of passed value */
  static size_t Size(const ValueType& x) { return sizeof(x); }
  /** Get the size of passed value */
  static size_t Size(const NoLinkType& x) { return sizeof(char); }
  /** Get the value of the service */ 
  static ValueType Value(DimInfo& i) { 
    char* v = reinterpret_cast<NoLinkType*>(i.getData());
    return ((*v) != 0 && (*v) != -1);
  }
  /** Check if info value is the same as no-link */
  static bool IsUp(const NoLinkType& nolink, DimInfo& i)
  {
    char* v = reinterpret_cast<NoLinkType*>(i.getData());
    return nolink != *v;
  }
  /** Compare for equality */
  static bool Compare(const ValueType& x, ValueType& y) 
  {
    return x == y;
  }
};
//____________________________________________________________________
/** Class template that wraps types */
template <>
struct InfoTrait<std::string>
{
  /** Value type */
  typedef std::string ValueType;
  /** No link type */
  typedef std::string NoLinkType;
  /** Create the default no-link value */
  static NoLinkType NoLink() { return std::string("no link"); }
  /** Get the address of the passed value */ 
  static void* Address(const ValueType& x) { 
    return reinterpret_cast<void*>(&(const_cast<ValueType&>(x)));
  }
  /** Get the size of passed value */
  static size_t Size(const ValueType& x) { return sizeof(char)*x.size(); }
  /** Get the value of the service */ 
  static ValueType Value(DimInfo& i) { 
    char* v = reinterpret_cast<char*>(i.getData());
    int   l = i.getSize();
    std::string r;
    for (int j = 0; j < l; j++) r.push_back(v[j]);
    return r;
  }
  /** Check if info value is the same as no-link */
  static bool IsUp(const NoLinkType& nolink, DimInfo& i)
  {
    return nolink != Value(i);
  }
  /** Compare for equality */
  static bool Compare(const ValueType& x, ValueType& y) 
  {
    return x == y;
  }
};


  

//____________________________________________________________________
/** Class template that wraps a DimInfo object */
template <typename T, typename Trait=InfoTrait<T> >
class Info 
{
public:
  /** Type of trait */
  typedef Trait             TraitType;
  /** Type of no-link value */
  typedef typename TraitType::NoLinkType NoLinkType;
  /** Type of value */
  typedef T                 ValueType;
  /** Constructor 
      @param name Name 
      @param handler Handler of updates 
      @param timeout Time out for calling info handler (0 means no
      timeout)
      @param nolink  The no-link value */
  Info(const char* name, DimInfoHandler* handler, int timeout=0, 
       const NoLinkType& nolink=TraitType::NoLink()) 
    : fInfo(name, timeout, 
	    TraitType::Address(nolink),
	    TraitType::Size(nolink),handler), 
      fNoLink(nolink)
  {
  }
  /** Get the name of the service 
      @return The name of the service */
  const char* Name() const { return fInfo.getName(); }
  /** Get the current value of the service 
      @return The current value */
  const ValueType& Value() const { return TraitType::Value(fInfo); }
  /** Get the time stamp of last update 
      @return Time stamp */
  const int& Timestamp() const { return fInfo.getTimestamp(); }
  /** Get the time stamp of last update 
      @param  ms On return, the millisecond part of the time stamp
      @return Time stamp */
  const int& Timestamp(int& ms) const 
  {
    ms   = fInfo.getTimestampMillisecs();
    return fInfo.getTimestamp();
  }
  const int Quality() const { return fInfo.getQuality(); }
  /** Get the service format 
      @return A text string describing the service */
  const char* Format() const { return fInfo.getFormat(); }
  /** Check if service is up by comparing to the no link value 
      @return true if the service is up (value isn't no link value) */ 
  bool IsUp() const { return TraitType::IsUp(fNoLink, fInfo); }
protected:
  /** Service subscribed to */
  mutable DimStampedInfo fInfo;
  /** The no link value */
  const NoLinkType fNoLink;
};

#endif
//
// EOF
//
