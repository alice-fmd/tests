------------------------------------------------------------------------------
-- Title      : Card switcher module
-- Project    : ALICE Read-out Controller Unit firmware.
------------------------------------------------------------------------------
-- File       : card_sw.vhdl
-- Author     : Christian Holm Christensen  <cholm@nbi.dk>
-- Company    : Niels Bohr Institute
-- Last update: 2007/04/20
-- Platform   : 
------------------------------------------------------------------------------
-- Description: Example of how to implement card switching in the RCU. 
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2007/04/20  1.0      cholm	Created
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity card_switcher is
  generic (
    DEL         :     integer := 40;    -- # clock cycles to wait for flip
    DUR         :     integer := 4);    -- # clock cycles to keep switch low
  port (
    clk         : in  std_logic;        -- Clock
    rstb        : in  std_logic;        -- Async reset (active low)
    reload_enb  : in  std_logic;        -- Reload command
    reload_addr : in  std_logic_vector(4 downto 0);  -- Card to reset
    actfec      : in  std_logic_vector(31 downto 0);  -- Active cards register
    card_sw     : out std_logic_vector(31 downto 0));  -- Card switches
end entity card_switcher;

architecture rtl of card_switcher is
  type state_t is (idle, turn_on, flip);  -- States

  signal state_i  : state_t;                        -- State
  signal target_i : std_logic_vector(31 downto 0);  -- Target value
  signal actfec_i : std_logic_vector(31 downto 0);  -- Cache of ACTFEC
  signal cardsw_i : std_logic_vector(31 downto 0);  -- What to set it to.
  signal delay_i  : integer;                        -- Delay counter
  signal down_i   : integer;                        -- Down counter
begin  -- architecture rtl
  -- purpose: Collect combinatorics
  combi           : block is
  begin  -- block combi
    card_sw <= cardsw_i;
  end block combi;

  -- purpose: State machine that togglees the card switches
  -- type   : sequential
  -- inputs : clk, rstb, reload_enb, reload_addr, actfec
  -- outputs: cardsw_i
  cardsw_fsm         : process (clk, rstb) is
    variable shift_i : natural;
    variable mask_i  : unsigned(31 downto 0);
  begin  -- process cardsw_fsm
    if rstb = '0' then                  -- asynchronous reset (active low)
      cardsw_i         <= (others        => '0');
      actfec_i         <= (others        => '0');
      target_i         <= (others        => '0');
      state_i          <= idle;
      delay_i          <= 0;
      down_i           <= 0;
    elsif clk'event and clk = '1' then  -- rising clock edge
      case state_i is
        when idle                        =>
          actfec_i     <= actfec;
          cardsw_i     <= actfec_i;
          delay_i      <= 0;
          down_i       <= 0;
          -- Check if value of ACTFEC has changed, and if so turn on/off the
          -- corresponding cards.   If a reload command was given, then check
          -- if the card is on, and if so, flick the card switch on that card
          -- only. 
          if (actfec /= actfec_i) then
            target_i   <= actfec and not actfec_i;
            state_i    <= turn_on;
          elsif reload_enb = '1' then
            shift_i           := to_integer(unsigned(reload_addr));
            -- don't reset cards that are off
            if actfec_i(shift_i) = '0' then
              state_i  <= idle;
            else
              mask_i          := (others => '0');
              mask_i(shift_i) := '1';
              target_i <= std_logic_vector(mask_i);
              state_i  <= flip;
            end if;
          end if;
        when turn_on                     =>
          -- Turn on/off the card
          cardsw_i     <= actfec_i;
          delay_i      <= delay_i + 1;
          if (delay_i >= DEL) then
            state_i    <= flip;
          end if;
        when flip                        =>
          -- Flip the card switch on the appropriate lines 
          cardsw_i     <= actfec_i xor target_i;
          down_i       <= down_i + 1;
          if (down_i >= DUR) then
            state_i    <= idle;
          end if;
        when others                      =>
          state_i      <= idle;
      end case;
    end if;
  end process cardsw_fsm;
end architecture rtl;

------------------------------------------------------------------------------
--
-- Test bench for card_switch module.
-- 
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity card_switcher_tb is
end entity card_switcher_tb;

------------------------------------------------------------------------------
architecture test of card_switcher_tb is
  constant DEL    : integer := 40;      -- # clock cycles to wait for flip
  constant DUR    : integer := 4;       -- # clock cycles to keep switch low
  constant PERIOD : time    := 25 ns;   -- Clock period

  signal clk_i         : std_logic                     := '0';
  signal rstb_i        : std_logic                     := '0';
  signal reload_enb_i  : std_logic                     := '0';
  signal reload_addr_i : std_logic_vector(4 downto 0)  := (others => '0');
  signal actfec_i      : std_logic_vector(31 downto 0) := (others => '0');
  signal card_sw_i     : std_logic_vector(31 downto 0);  -- Card switches
  signal i             : integer;                        -- counter

begin  -- architecture test
  -- purpose: Provide stimuli to dut
  -- type   : combinational
  -- inputs : 
  -- outputs: reload_enb_i, reload_addr_i, actfec_i
  stimuli: process is
    variable mask_i : unsigned(31 downto 0);
  begin  -- process stimuli
    wait for 4 * PERIOD;
    rstb_i <= '1';
    wait until rising_edge(clk_i);
    -- End of initial reset

    -- Turn on one card only 
    for i in 0 to 31 loop
      mask_i    := (others => '0');
      mask_i(i) := '1';
      report "Turning on card # " & integer'image(i) severity note;
      wait until rising_edge(clk_i);
      actfec_i <= std_logic_vector(mask_i);
      wait for (DEL+DUR+10) * PERIOD;
    end loop;  -- i

    -- Turning of all cards 
    wait for 100 ns;
    report "turn of all cards" severity note;
    actfec_i <= (others => '0');
    wait for 100 ns;
    

    -- Turn on progessively more cards 
    for i in 0 to 31 loop
      mask_i    := (others => '0');
      mask_i(i) := '1';
      report "Turning on card # " & integer'image(i) severity note;
      wait until rising_edge(clk_i);
      actfec_i <= actfec_i or std_logic_vector(mask_i);
      wait for (DEL+DUR+10) * PERIOD;
    end loop;  -- i

    -- Wait a while 
    wait for 200 ns;

    -- Reset each card 
    for i in 0 to 31 loop
      report "Reloading card # " & integer'image(i) severity note;
      wait until rising_edge(clk_i);
      reload_addr_i <= std_logic_vector(to_unsigned(i, 5));
      wait until rising_edge(clk_i);
      reload_enb_i <= '1';
      wait until rising_edge(clk_i);
      reload_enb_i <= '0';
      wait for (DEL+DUR+4) * PERIOD;
    end loop;  -- i
    
    -- Turning of all cards 
    wait for 100 ns;
    report "turn of all cards" severity note;
    actfec_i <= (others => '0');
    wait for 100 ns;

    -- Reset each card - does nothing
    for i in 0 to 31 loop
      report "Reloading card # " & integer'image(i) severity note;
      wait until rising_edge(clk_i);
      reload_addr_i <= std_logic_vector(to_unsigned(i, 5));
      wait until rising_edge(clk_i);
      reload_enb_i <= '1';
      wait until rising_edge(clk_i);
      reload_enb_i <= '0';
      wait for 8 * PERIOD;
    end loop;  -- i
    
    -- Turning on all cards 
    wait for 100 ns;
    report "turn of all cards" severity note;
    actfec_i <= (others => '1');
    wait for 100 ns;

    -- Turn on progessively more cards 
    for i in 0 to 31 loop
      mask_i    := (others => '0');
      mask_i(i) := '1';
      report "Turning off card # " & integer'image(i) severity note;
      wait until rising_edge(clk_i);
      actfec_i <= actfec_i and not std_logic_vector(mask_i);
      wait for (DEL+DUR+10) * PERIOD;
    end loop;  -- i
    
    wait;                               -- forever 
  end process stimuli;

  -- purpose: Make a clock
  -- type   : combinational
  -- inputs : 
  -- outputs: clk_i
  clocker: process is
  begin  -- process clocker
    clk_i <= not clk_i;
    wait for PERIOD / 2;
  end process clocker;
  
  dut : entity work.card_switcher
    generic map (
      DEL         => DEL,               -- # clock cycles to wait for flip
      DUR         => DUR)               -- # clock cycles to keep switch low
    port map (
      clk         => clk_i,             -- in  Clock
      rstb        => rstb_i,            -- in  Async reset (active low)
      reload_enb  => reload_enb_i,      -- in  Reload command
      reload_addr => reload_addr_i,     -- in  Card to reset
      actfec      => actfec_i,          -- in  Active cards register
      card_sw     => card_sw_i);        -- out Card switches

end architecture test;
------------------------------------------------------------------------------
--
-- EOF
--
